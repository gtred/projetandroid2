package com.example.ProjetAndroid;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.List;

public interface PandaScoreClient {

    @GET("/lol/teams")
    Call<List<Champion>> retourneEquipeDetails(@Query("filter[name]") String name, @Query("token") String token);

    @GET("/lol/teams?token=J5AK5SIjS1YVfpd3mXn5wXMEpdyMMYni4M6Ljn7yqPAPX8bJVTU")
    Call<List<Champion>> retourneEquipes();

    @GET("/lol/players")
    Call<List<Player>> retourneJoueurDetails(@Query("filter[id]") int id, @Query("token") String token);

    @GET("/lol/teams")
    Call<List<Team>> retourneEquipe(@Query("filter[acronym]") String acronym, @Query("token") String token);

    @GET("/lol/teams")
    Call<List<Player>> retourneJoueursEquipe(@Query("filter[id]") int id, @Query("token") String token);

    @GET("/lol/players?token=J5AK5SIjS1YVfpd3mXn5wXMEpdyMMYni4M6Ljn7yqPAPX8bJVTU")
    Call<List<Champion>> retourneJoueurs();

    @GET("/lol/champions")
    Call<List<Champion>> retourneChampionDetails(@Query("filter[name]") String name, @Query("token") String token);

    @GET("/lol/champions?token=J5AK5SIjS1YVfpd3mXn5wXMEpdyMMYni4M6Ljn7yqPAPX8bJVTU")
    Call<List<Champion>> retourneChampions();

    @GET("/lol/matches")
    Call<List<Game>> retournePartieDetails( @Query("filter[league_id]") String league_id, @Query("token") String token);
}

