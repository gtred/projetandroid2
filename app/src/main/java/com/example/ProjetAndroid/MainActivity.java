package com.example.ProjetAndroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import retrofit2.*;
import retrofit2.converter.gson.*;
import java.util.*;

import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private String token = "J5AK5SIjS1YVfpd3mXn5wXMEpdyMMYni4M6Ljn7yqPAPX8bJVTU";
    private String league_id = "293"; //LCK

    private Application ap = new Application();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.pandascore.co")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        final PandaScoreClient client = retrofit.create(PandaScoreClient.class);

        final Call<List<Game>> call = client.retournePartieDetails(league_id, token);


        call.enqueue(new Callback<List<Game>>() {

            @Override
            public void onResponse(Call<List<Game>> call, Response<List<Game>> response) {

                ap.setListeParties(response.body());

                Call<List<Team>> call2 = client.retourneEquipe(ap.getListeParties().get(0).getOpponent1().getAcronym(), token);

                call2.enqueue(new Callback<List<Team>>() {

                    @Override
                    public void onResponse(Call<List<Team>> call, Response<List<Team>> response) {

                        ap.setTeam1(response.body().get(0));

                        Call<List<Team>> call3 = client.retourneEquipe(ap.getListeParties().get(0).getOpponent2().getAcronym(), token);

                        call3.enqueue(new Callback<List<Team>>() {

                            @Override
                            public void onResponse(Call<List<Team>> call, Response<List<Team>> response) {

                                ap.setTeam2(response.body().get(0));

                                ArrayList<Team> liste = new ArrayList<Team>();
                                liste.add(ap.getTeam1());
                                liste.add(ap.getTeam2());
                                ap.setListeEquipe(liste);

                                RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                                myRecyclerView.setAdapter( new MyAdapter(ap));
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

                            }

                            @Override
                            public void onFailure(Call<List<Team>> call, Throwable t) {
                                Toast.makeText(MainActivity.this, "Error...!!!", Toast.LENGTH_SHORT).show();
                            }

                        });



                    }

                    @Override
                    public void onFailure(Call<List<Team>> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Error...!!!", Toast.LENGTH_SHORT).show();
                    }

                });

            }

            @Override
            public void onFailure(Call<List<Game>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error...!!!", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
