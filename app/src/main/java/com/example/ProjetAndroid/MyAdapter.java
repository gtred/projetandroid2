package com.example.ProjetAndroid;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyViewHolder> {

    private Application application;

    public MyAdapter(Application application) {

        this.application = application;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if(position % 2 == 0){
            holder.itemView.setBackgroundColor(Color.parseColor("#7fb3d5"));
        }else{
            holder.itemView.setBackgroundColor(Color.parseColor("#ec7063"));
        }
        holder.display(application.getListeParties().get(position), application.getListeTeam().get(0).getPlayers(), application.getListeTeam().get(1).getPlayers());

    }

    @Override
    public int getItemCount() {
        return application.getListeParties().size() + application.getTeam1().getPlayers().size() + application.getTeam2().getPlayers().size();
    }

}
