package com.example.ProjetAndroid;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class MyViewHolder extends RecyclerView.ViewHolder {

    private TextView textViewTeamName1;
    private TextView textViewTeamName2;
    private ImageView imageViewOpponent1;
    private ImageView imageViewOpponent2;
    private TextView textViewGameName;
    private TextView textViewDate;

    // Equipe #1
    private ImageView imageViewRole1Team1;
    private ImageView imageViewRole2Team1;
    private ImageView imageViewRole3Team1;
    private ImageView imageViewRole4Team1;
    private ImageView imageViewRole5Team1;

    // Equipe #2
    private ImageView imageViewRole1Team2;
    private ImageView imageViewRole2Team2;
    private ImageView imageViewRole3Team2;
    private ImageView imageViewRole4Team2;
    private ImageView imageViewRole5Team2;


    public MyViewHolder(@NonNull View itemView) {
        super(itemView);
        textViewTeamName1 = itemView.findViewById(R.id.textViewTeamName1);
        textViewTeamName2 = itemView.findViewById(R.id.textViewTeamName2);
        imageViewOpponent1 = itemView.findViewById(R.id.imageViewOpponent1);
        imageViewOpponent2 = itemView.findViewById(R.id.imageViewOpponent2);
        textViewGameName = itemView.findViewById(R.id.textViewGameName);
        textViewDate = itemView.findViewById(R.id.textViewDate);

        imageViewRole1Team1 = itemView.findViewById(R.id.imageViewRole1Team1);
        imageViewRole2Team1 = itemView.findViewById(R.id.imageViewRole2Team1);
        imageViewRole3Team1 = itemView.findViewById(R.id.imageViewRole3Team1);
        imageViewRole4Team1 = itemView.findViewById(R.id.imageViewRole4Team1);
        imageViewRole5Team1 = itemView.findViewById(R.id.imageViewRole5Team1);

        imageViewRole1Team2 = itemView.findViewById(R.id.imageViewRole1Team2);
        imageViewRole2Team2 = itemView.findViewById(R.id.imageViewRole2Team2);
        imageViewRole3Team2 = itemView.findViewById(R.id.imageViewRole3Team2);
        imageViewRole4Team2 = itemView.findViewById(R.id.imageViewRole4Team2);
        imageViewRole5Team2 = itemView.findViewById(R.id.imageViewRole5Team2);
    }

    void display(Game game, List<Player> player1, List<Player> player2){

        textViewTeamName1.setText(game.getOpponent1().getName());

        textViewTeamName2.setText(game.getOpponent2().getName());
        textViewGameName.setText(game.getGameName());
        textViewDate.setText(game.getBegin_at());

        Glide.with(imageViewOpponent1)
                .load(game.getOpponent1().getImage_url())
                .into(imageViewOpponent1);

        Glide.with(imageViewOpponent2)
                .load(game.getOpponent2().getImage_url())
                .into(imageViewOpponent2);

        // Equipe #1
        Glide.with(imageViewRole1Team1)
                .load(player1.get(0).getImage_url())
                .into(imageViewRole1Team1);

        Glide.with(imageViewRole2Team1)
                .load(player1.get(1).getImage_url())
                .into(imageViewRole2Team1);

        Glide.with(imageViewRole3Team1)
                .load(player1.get(2).getImage_url())
                .into(imageViewRole3Team1);

        Glide.with(imageViewRole4Team1)
                .load(player1.get(3).getImage_url())
                .into(imageViewRole4Team1);

        Glide.with(imageViewRole5Team1)
                .load(player1.get(4).getImage_url())
                .into(imageViewRole5Team1);

        // Equipe 2
        Glide.with(imageViewRole1Team2)
                .load(player2.get(0).getImage_url())
                .into(imageViewRole1Team2);

        Glide.with(imageViewRole2Team2)
                .load(player2.get(1).getImage_url())
                .into(imageViewRole2Team2);

        Glide.with(imageViewRole3Team2)
                .load(player2.get(2).getImage_url())
                .into(imageViewRole3Team2);

        Glide.with(imageViewRole4Team2)
                .load(player2.get(3).getImage_url())
                .into(imageViewRole4Team2);

        Glide.with(imageViewRole5Team2)
                .load(player2.get(4).getImage_url())
                .into(imageViewRole5Team2);
    }
}