package com.example.ProjetAndroid;

import java.util.List;

public class Team {

    private String acronym;
    private String id;
    private String name;
    private String image_url;
    private List<Player> players;

    public String getAcronym() {
        return acronym;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage_url() {
        return image_url;
    }

    public List<Player> getPlayers() {
        return players;
    }

}
