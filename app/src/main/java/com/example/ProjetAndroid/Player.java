package com.example.ProjetAndroid;

public class Player {

    private String id;
    private String name;
    private String first_name;
    private String last_name;
    private String role;
    private String nationality;
    private String image_url;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getRole() {
        return role;
    }

    public String getNationality() {
        return nationality;
    }

    public String getImage_url() {
        return image_url;
    }
}
