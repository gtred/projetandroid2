package com.example.ProjetAndroid;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Game {

    private String name;
    private String id;
    private ArrayList opponents;
    private Opponent opponent1;
    private Opponent opponent2;
    private Team team1;
    private Team team2;
    private String begin_at;

    public String getGameName() {
        return name;
    }
    public String getGameId() {
        return id;
    }
    public ArrayList getOpponents() { return opponents; }

    public void setOpponent1(){

        LinkedTreeMap<String, Object> ltm = (LinkedTreeMap<String, Object>) this.getOpponents().get(0);
        LinkedTreeMap<String, String> ltm2 = (LinkedTreeMap<String, String>) ltm.get("opponent");
        Opponent op = new Opponent();
        op.setAcronym(ltm2.get("acronym"));
        op.setImage_url(ltm2.get("image_url"));
        op.setName(ltm2.get("name"));
        //op.setId(ltm2.get("id"));
        this.opponent1 = op;
    }

    public void setOpponent2(){

        LinkedTreeMap<String, Object> ltm = (LinkedTreeMap<String, Object>) this.getOpponents().get(1);
        LinkedTreeMap<String, String> ltm2 = (LinkedTreeMap<String, String>) ltm.get("opponent");
        Opponent op = new Opponent();
        op.setAcronym(ltm2.get("acronym"));
        op.setImage_url(ltm2.get("image_url"));
        op.setName(ltm2.get("name"));
        //String test = ltm2.get(id);
        //String test2 = ltm2.get("id");
        //op.setId(ltm2.get(id));
        this.opponent2 = op;
    }

    public Opponent getOpponent1() {
        setOpponent1();
        return opponent1;
    }

    public Opponent getOpponent2() {
        setOpponent2();
        return opponent2;
    }

    public String getBegin_at(){
        return this.begin_at;
    }
}
