package com.example.ProjetAndroid;

import java.util.ArrayList;
import java.util.List;

public class Application {

    private List<Game> listeParties;
    private List<Team> listeTeam;
    private Team team1;
    private Team team2;

    public List<Game> getListeParties() {
        return listeParties;
    }

    public void setListeParties(List<Game> listeParties) {
        this.listeParties = listeParties;
    }

    public List<Team> getListeTeam() {
        return listeTeam;
    }

    public Team getTeam1() { return team1; }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() { return team2; }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    public void addTeam(Team team) {
        this.listeTeam.add(team);
    }

    public void setListeEquipe(List<Team> team) {
        this.listeTeam = team;
    }

    public Application(){
        listeTeam = new ArrayList<Team>();
    }
}
